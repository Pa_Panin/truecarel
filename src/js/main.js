$(document).ready(function () {
  $(".owl-carousel").owlCarousel({
    nav: true,
    loop: true,
    items: 6,
    autoplay: true,
    navText: ['<img src="../prev.svg">', '<img src="../next.svg">'],
    responsiveClass: true,
    responsive: {
      // от 0 и больше
      0: {
        items: 1,
        nav: false,
        autoplay: true,
      },
      // от 600 и больше
      600: {
        items: 2,
      },
      768: {
        items: 3,
      },
      // от 1000 и больше
      1000: {
        items: 5,
        nav: true,
        loop: true,
        autoplay: true,
      },
      1200: {
        items: 6,
      },
    },
  });
});

$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() != 0) {
      $("#toTop").fadeIn();
    } else {
      $("#toTop").fadeOut();
    }
  });

  $("#toTop").click(function () {
    $("body,html").animate({ scrollTop: 0 }, 500);
  });
});
