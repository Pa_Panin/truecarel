const $ = require("jquery");

window.jQuery = $;

if ($) {
  require("bootstrap");
  require("@fancyapps/fancybox");
  require("owl.carousel");
  require("./src/scss/required.scss");
  require("./src/js/main.js");
}
